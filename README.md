# SIREN Technical Assesment

### Problem:

> At Facebook, you have friends who all have a list of movies that they like. Write a function that returns the most popular movie of a person in his/her network of friends. Network means that you have to consider friends of friends as well.

## Setup

npm install

## Run

npm test
