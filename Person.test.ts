import assert from 'assert'

import Person from './Person'
import { NetworkLevels } from './Person'

// Global Arrange
// Friends of My Friends
const athena = new Person([], ["Kiki's Delivery Service", "Bohemian Rhapsody"])
const demeter = new Person([], ["Dune"])
const poseidon = new Person([], ["The Social Network", "Bohemian Rhapsody"])
// My Friends
const zeus = new Person([poseidon], ["Dune", "Firefly", "The Social Network"])
const hera = new Person([demeter, athena], ["Kiki's Delivery Service", "Bohemian Rhapsody"])
// Me
const me = new Person([zeus, hera], ["Nightmare Before Christmas", "Bohemian Rhapsody", "Firefly"])

function test_findHighestMovieCount_FriendsOfMyFriends() {
    // Arrange
    // Act
    const actual = me.findHighestMovieCount(NetworkLevels.FriendsOfMyFriends)

    // Assert
    const expected = ['Bohemian Rhapsody']
    try {
        assert.deepStrictEqual(actual, expected)
        console.log(test_findHighestMovieCount_FriendsOfMyFriends.name + ' passed')
    } catch (e) {
        console.log(e)
    }
}

function test_findHighestMovieCount_MyFriends() {
    // Arrange
    // Act
    const actual = me.findHighestMovieCount(NetworkLevels.MyFriends)

    // Assert
    const expected = ['Bohemian Rhapsody', 'Firefly']
    try {
        assert.deepStrictEqual(actual, expected)
        console.log(test_findHighestMovieCount_MyFriends.name + ' passed')
    } catch (e) {
        console.log(e)
    }
}

function test_findHighestMovieCount_OnlyMe() {
    // Arrange
    // Act
    const actual = me.findHighestMovieCount(NetworkLevels.OnlyMe)

    // Assert
    const expected = ['Nightmare Before Christmas', 'Bohemian Rhapsody', 'Firefly']
    try {
        assert.deepStrictEqual(actual, expected)
        console.log(test_findHighestMovieCount_OnlyMe.name + ' passed')
    } catch (e) {
        console.log(e)
    }
}

test_findHighestMovieCount_FriendsOfMyFriends()
test_findHighestMovieCount_MyFriends()
test_findHighestMovieCount_OnlyMe()
