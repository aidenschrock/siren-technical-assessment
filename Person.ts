export enum NetworkLevels {
    OnlyMe,
    MyFriends,
    FriendsOfMyFriends
}


export default class Person {

    private friends: Person[];
    private movies: string[];

    constructor(friends: Person[], movies: string[]) {
        this.friends = friends;
        this.movies = movies;
    }


    private queryFriendsMovies(searchDepth: number): any {

        let movies: string[]
        if (searchDepth === 0) {

            movies = this.movies
        } else {
            movies = this.friends.reduce((acc, friend) => {
                return acc.concat(friend.queryFriendsMovies(searchDepth - 1))
            }, this.movies)
        }

        return movies
    }


    public findHighestMovieCount(searchDepth: NetworkLevels): string[] {

        let movies = this.queryFriendsMovies(searchDepth)

        let counts = movies.reduce((countObj: any, title: string) => {
            countObj[title] = countObj[title] ? countObj[title] + 1 : 1;
            return countObj
        }, {})

        const maxValue = Math.max(...Object.keys(counts).map(x => counts[x]))

        let highestMovieCounts = Object.keys(counts).filter(x => counts[x] === maxValue)

        return highestMovieCounts
    }
}
